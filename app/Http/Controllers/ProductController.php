<?php

namespace App\Http\Controllers;

use App\Exports\ProductExport;
use App\Imports\ProductImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use DNS1D;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use DataTables;
use Excel;
use DateTime;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'globalSettings']);
    }

    public function product_expiry_check(Request $req){
        $useInterval    = $req->interval;
        $result         = [];

        if(empty($useInterval) || $useInterval == false){
            $products = DB::table('products')->where("expired_at", "<", date("Y-m-d"));
            if(Auth::user()->role != 0){
                $products = $products->where("products.store_id", Auth::user()->store_id);
            }
        } else {
            $interval = Auth::user()->exp_reminder;
            $products = DB::table('products')->whereBetween("expired_at", [date("Y-m-d"), date("Y-m-d", strtotime(date("Y-m-d").' +'.$interval.' day'))]);
            if(Auth::user()->role != 0){
                $products = $products->where("products.store_id", Auth::user()->store_id);
            }
        }

        if(Auth::user()->role != 0){
            $products = $products->where("products.store_id", Auth::user()->store_id);
        }

        $products   = $products->get();
        
        $result = ["count" => $products->count()];

        return response()->json($result);
    }

    public function products(Request $req){
        if ($req->ajax()) {
            $products = DB::table('products')
                                ->leftJoin('stores', 'products.store_id', 'stores.store_id')
                                ->select('products.*', 'stores.store_name');

            if($req->status == 1){
                $products = $products->where("expired_at", "<", date("Y-m-d"));
                if(Auth::user()->role != 0){
                    $products = $products->where("products.store_id", Auth::user()->store_id);
                }
            } else if($req->status == 2){
                $interval = Auth::user()->exp_reminder;
                $products = $products->whereBetween("expired_at", [date("Y-m-d"), date("Y-m-d", strtotime(date("Y-m-d").' +'.$interval.' day'))]);
                if(Auth::user()->role != 0){
                    $products = $products->where("products.store_id", Auth::user()->store_id);
                }
            } else {
                if(Auth::user()->role != 0){
                    $products = $products->where("products.store_id", Auth::user()->store_id);
                }
            }
            
            $data = $products->get();

            foreach($data as $d){
                $d->expired_at = date("d/m/Y", strtotime($d->expired_at));
            }

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('price', function($row){
                        return number_format($row->price, 2, ',', '.');
                    })
                    ->addColumn('gross', function($row){
                        return number_format($row->qty*$row->price, 2, ',', '.');
                    })
                    ->addColumn('action', function($row){   
                        $btn = '<button title="Edit Produk" type="button" class="editProductPopup btn btn-success btn-xs" data-toggle="modal" data-target="#add-product" onclick=\'editProduct('.json_encode($row).')\'><i class="fas fa-edit"></i></button>';   
                        $btn = $btn.' <button title="Hapus Produk" type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-product" onclick=\'deleteProductConfirm('.json_encode($row).')\'><i class="fas fa-trash"></i></button>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return View::make("products");
    }

    public function product_save(Request $req){
        if(Auth::user()->role == 0){
            $req->validate([
                'store' => 'required|numeric',
                
            ],
            [
                'store.required'    => 'Toko belum dipilih!',
                'store.numeric'     => 'Terjadi kesalahan! Mohon coba kembali!',
            ]);

            $store_id = $req->store;
        } else {
            $store_id = Auth::user()->store_id;
        }

        $req->validate([
            'product_code'      => 'required|numeric',
            'product_name'      => 'required',
            'qty'               => 'required|numeric',
            'price'             => 'numeric',
            'expired_at'        => 'required',
            
        ],
        [
            'product_code.required'     => 'Kode Produk belum diisi!',
            'product_code.numeric'      => 'Kode Produk harus berupa angka!',
            'product_code.unique'       => 'Kode Produk telah digunakan!',
            'product_name.required'     => 'Nama Produk belum diisi!',
            'qty.required'              => 'Qty belum diisi!',
            'qty.numeric'               => 'Qty harus berupa angka!',
            'price.numeric'             => 'Harga harus berupa angka!',
            'expired_at.required'       => 'Tanggal expired belum dipilih!',
        ]);

        $data = [
            "store_id"          => $store_id,
            "product_code"      => $req->product_code,
            "product_name"      => $req->product_name,
            "qty"               => $req->qty,
            "price"             => $req->price,
            "expired_at"        => date("Y-m-d", strtotime(str_replace("/","-",$req->expired_at))),
        ];

        if(empty($req->id)){
            $add = DB::table('products')->insertGetId($data);

            if($add){
                $result = ["success" => "Product berhasil ditambahkan."];
            } else {
                $result = ["error" => "Product gagal ditambahkan."];
            }
        } else {
            $update = DB::table('products')->where("product_id", $req->id)->update($data);

            if($update){
                $result = ["success" => "Product berhasil diedit."];
            } else {
                $result = ["error" => "Product gagal diedit."];
            }
        }
        
        return response()->json($result);
    }

    public function product_delete(Request $req){
        $del = DB::table('products')->where("product_id", $req->id)->delete();

        if($del){
            $result = ["success" => "Product berhasil dihapus."];
        } else {
            $result = ["error" => "Product gagal dihapus."];
        }

        return response()->json($result);
    }

    public function product_import(Request $req){
        $store_id = $req->store;

		$this->validate($req, [
			'file' => 'required|mimetypes:text/plain,text/csv,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.oasis.opendocument.spreadsheet,application/zip'
        ],
        [
            "file.required"     => "File belum dipilih!",
            "file.mimetypes"    => "File harus dalam format CSV/ODS/XLS/XLSX!"
        ]);
 
		$file = $req->file('file');
 
		$filename = rand().$file->getClientOriginalName();
 
		$file->move('upload/',$filename);
 
		$import = Excel::toArray(new ProductImport, public_path('upload/'.$filename));

        $data = [];
        foreach($import as $value){
            foreach($value as $v){
                $data[]=$v;
            }
        }

        $doneImport = 0;
        $countImport = count($data);
        
        foreach($data as $d){
            if(is_float(($d['TGL_EXPIRED']))){
                $expired_at = gmdate("d/m/Y", ($d['TGL_EXPIRED'] - 25569) * 86400);
            } else {
                $expired_at = date("d/m/Y", strtotime($d['TGL_EXPIRED']));
            }
            $param = new Request([
                'store'             => $store_id,
                'product_code'      => $d['KLS'],
                'product_name'      => $d['DESC'],
                'qty'               => "".$d['QTY'],
                'expired_at'        => $expired_at,
            ]);

            $add = $this->product_save($param);

            if($add){
                $doneImport++;
            }
        }

        if($doneImport == $countImport){
            $result = ["success" => "Semua data berhasil diimport."];
        } else {
            if($doneImport > 0){
                $result = ["success" => "Sebagian data berhasil diimport."];
            } else {
                $result = ["error" => "Data gagal diimport."];
            }
        }
        
		return response()->json($result);
    }

    public function product_export(Request $req){
        $format     = $req->format;

        if(Auth::user()->role == 0){
            $store_id   = $req->store;
        } else {
            $store_id   = Auth::user()->store_id;
        }

        $products   = DB::table('products')->where("store_id", $store_id)->select("*")->get();

        if($products->count() > 0){
            $data = [];
            foreach($products as $d){
                $data[] = [
                    "KLS"           => strval($d->product_code),
                    "DESC"          => strval($d->product_name),
                    "QTY"           => strval($d->qty),
                    "PRICE"         => strval($d->price),
                    "GROSS"         => strval($d->qty*$d->price),
                    "TGL_EXPIRED"   => date("m/d/Y", strtotime($d->expired_at)),
                ];
            }

            if($format == "xls"){
                return (new ProductExport($data))->download('products.xls', \Maatwebsite\Excel\Excel::XLS);
            } else if($format == "xlsx"){
                return (new ProductExport($data))->download('products.xlsx', \Maatwebsite\Excel\Excel::XLSX);
            } else if($format == "csv"){
                return (new ProductExport($data))->download('products.csv', \Maatwebsite\Excel\Excel::CSV, [
                    'Content-Type' => 'text/csv',
                ]);
            } else if($format == "ods"){
                return (new ProductExport($data))->download('products.ods', \Maatwebsite\Excel\Excel::ODS, [
                    'Content-Type' => 'application/vnd.oasis.opendocument.spreadsheet',
                ]);
            } else {
                $req->session()->flash("error", "Format tidak tersedia!!");
                return redirect()->route('products');
            }
        } else {
            $req->session()->flash("error", "Tidak ada data untuk di-export!");
            return redirect()->route('products');
        }
    }
}
