<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false, 'reset' => false, 'verify' => false]);
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard')->middleware('auth');

Route::prefix('products')->group(function () {
    Route::get('', [App\Http\Controllers\ProductController::class, 'products'])->name('products');
    Route::post('', [App\Http\Controllers\ProductController::class, 'product_save'])->name('products.save');
    Route::delete('', [App\Http\Controllers\ProductController::class, 'product_delete'])->name('products.delete');
    Route::post('import', [App\Http\Controllers\ProductController::class, 'product_import'])->name('products.import');
    Route::post('export', [App\Http\Controllers\ProductController::class, 'product_export'])->name('products.export');
    Route::get('expiryCheck', [App\Http\Controllers\ProductController::class, 'product_expiry_check'])->name('products.expiryCheck');
});

Route::prefix('settings')->group(function () {
    Route::get('reminder', [App\Http\Controllers\UserController::class, 'reminder'])->name('setting.reminder');
    
    Route::prefix('stores')->group(function () {
        Route::get('', [App\Http\Controllers\StoreController::class, 'stores'])->name('setting.stores')->middleware('adminRole');
        Route::delete('', [App\Http\Controllers\StoreController::class, 'store_delete'])->name('setting.store.delete')->middleware('adminRole');
        Route::post('', [App\Http\Controllers\StoreController::class, 'store_save'])->name('setting.store.save')->middleware('adminRole');
    });

    Route::prefix('users')->group(function () {
        Route::get('', [App\Http\Controllers\UserController::class, 'users'])->name('setting.users')->middleware('adminRole');
        Route::delete('', [App\Http\Controllers\UserController::class, 'user_delete'])->name('setting.users.delete')->middleware('adminRole');
        Route::post('', [App\Http\Controllers\UserController::class, 'user_save'])->name('setting.users.save')->middleware('adminRole');
    });

    Route::prefix('account')->group(function () {
        Route::get('', [App\Http\Controllers\UserController::class, 'myaccount'])->name('setting.account');
        Route::post('profile', [App\Http\Controllers\UserController::class, 'myaccount_update'])->name('setting.account.update');
        Route::post('password', [App\Http\Controllers\UserController::class, 'myaccount_update_password'])->name('setting.account.updatePassword');
    });
});