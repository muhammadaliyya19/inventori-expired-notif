<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
class GlobalSettings
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->role == 0){
                $store_name = "Admin";
            } else {
                $store_name = DB::table('stores')->where('store_id', Auth::user()->store_id)->first()->store_name;
            }
        } else {
            $store_name = Config::get('app.name');
        }

        view()->share('store_name', $store_name);

        return $next($request);
    }
}
