@extends('layouts.main')
@if(Request::get('status') == 1)
    @section('title', __('Daftar Produk Expired'))
@elseif(Request::get('status') == 2)
    @section('title', __('Daftar Segera Expired'))
@else
    @section('title', __('Daftar Produk'))
@endif
@section('custom-css')
    <link rel="stylesheet" href="/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
        </div>
        </div>
    </div>
    <section class="content">
    <div class="container-fluid">
        <div class="card">
            @if(empty(Request::get('status')) || Request::get('status') == 0)
            <div class="card-header">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-product" onclick="addProduct()"><i class="fas fa-plus"></i> Tambah Produk</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#import-product" onclick="importProductForm()"><i class="fas fa-file-import"></i> Import</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#export-product" onclick="exportProductForm()"><i class="fas fa-file-export"></i> Export</button>
            </div>
            @endif
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table" class="table data-table dt-head-center table-sm table-bordered table-hover table-striped">
                        <thead class="text-center">
                            <tr>
                                <th>No.</th>
                                <th>{{ __('Toko') }}</th>
                                <th>{{ __('Kode Produk') }}</th>
                                <th>{{ __('Nama Produk') }}</th>
                                <th>{{ __('Qty') }}</th>
                                <th>{{ __('Harga (Rp)') }}</th>
                                <th>{{ __('Gross (Rp)') }}</th>
                                <th>{{ __('Tanggal Expired') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-product">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modal-title" class="modal-title">{{ __('Tambahkan Produk') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form role="form" id="save" action="{{ route('products.save') }}" method="post">
                        @csrf
                        <input type="hidden" id="save_id" name="id">
                        <div class="form-group row">
                            <label for="product_code" class="col-sm-4 col-form-label">{{ __('Kode Produk') }}</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="product_code" name="product_code">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="product_name" class="col-sm-4 col-form-label">{{ __('Nama Produk') }}</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="product_name" name="product_name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="qty" class="col-sm-4 col-form-label">{{ __('Qty') }}</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="qty" name="qty">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price" class="col-sm-4 col-form-label">{{ __('Harga') }} (Rp)</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="price" name="price">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="expired_at" class="col-sm-4 col-form-label">Tanggal Expired</label>
                            <div class="col-sm-8">
                                <div class="input-group date" id="expired_at" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" id="expired_at_txt" data-target="#expired_at"/>
                                    <div class="input-group-append" data-target="#expired_at" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(Auth::user()->role == 0)
                        <div id="select-store" class="form-group row">
                            <label for="store" class="col-sm-4 col-form-label">Toko</label>
                            <div class="col-sm-8">
                                <select class="form-control select2" style="width: 100%;" id="store" name="store">
                                </select>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="button-save" type="button" class="btn btn-primary" data-dismiss="modal" onclick="saveProduct()">{{ __('Tambahkan') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="import-product">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Import</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <form role="form" enctype="multipart/form-data" files="true" id="import" action="{{ route('products.import') }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="file" class="col-sm-4 col-form-label">File</label>
                            <div class="col-sm-8 custom-file">
                                <input type="file" class="custom-file-input" id="file" name="file">
                                <label class="custom-file-label" for="file">Choose file</label>
                            </div>
                        </div>
                        @if(Auth::user()->role == 0)
                        <div class="form-group row">
                            <label for="store-import" class="col-sm-4 col-form-label">Toko</label>
                            <div class="col-sm-8">
                                <select class="form-control select2" style="width: 100%;" id="store-import" name="store">
                                </select>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Batal') }}</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="importProduct()">{{ __('Import') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="export-product">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Export</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <form id="export" action="{{ route('products.export') }}" method="post">
                        @csrf
                        @if(Auth::user()->role == 0)
                        <div class="form-group row">
                            <label for="store-export" class="col-sm-4 col-form-label">Toko</label>
                            <div class="col-sm-8">
                                <select class="form-control select2" style="width: 100%;" id="store-export" name="store">
                                </select>
                            </div>
                        </div>
                        @endif
                        <div class="form-group row">
                            <label for="export-format" class="col-sm-4 col-form-label">Format</label>
                            <div class="col-sm-8">
                                <select class="form-control select2" style="width: 100%;" id="export-format" name="format">
                                    <option value="csv">CSV</option>
                                    <option value="ods">ODS</option>
                                    <option value="xls" selected>XLS</option>
                                    <option value="xlsx">XLSX</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Batal') }}</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="$('#export').submit()">{{ __('Export') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete-product">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modal-title" class="modal-title">{{ __('Delete Product') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        @csrf
                        @method('delete')
                        <input type="hidden" id="delete_id" name="id">
                    </form>
                    <div>
                        <p>Anda yakin ingin menghapus product code <span id="pcode" class="font-weight-bold"></span> dari <span id="delete_store_name" class="font-weight-bold"></span>?</p>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="button-save" type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteProduct()">{{ __('Ya, hapus') }}</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('custom-js')
    <script src="/plugins/toastr/toastr.min.js"></script>
    <script src="/plugins/moment/moment.min.js"></script>
    <script src="/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <script src="/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="/plugins/select2/js/select2.full.min.js"></script>
    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <script>
        $('.select2').select2({
            theme: 'bootstrap4'
        });

        bsCustomFileInput.init();

        $('#expired_at').datetimepicker({
            viewMode: 'years',
            format: 'DD/MM/YYYY'
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var table = $('#table').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                "url": "{{ route('products') }}",
                "data": {
                    "status":"{{ Request::get('status') }}"
                }
            },
            'columnDefs': [
                {"targets": [0,4,7], "className": "text-center"}
            ],
            initComplete: function () {
                var api = this.api();
                var role = "{{ Auth::user()->role }}";
                if (role == 0) {
                    api.column(1).visible(true);
                } else {
                    api.column(1).visible(false);
                }
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'store_name', name: 'store_name'},
                {data: 'product_code', name: 'product_code'},
                {data: 'product_name', name: 'product_name'},
                {data: 'qty', name: 'qty'},
                {data: 'price', name: 'price'},
                {data: 'gross', name: 'gross'},
                {data: 'expired_at', name: 'expired_at'},
                {data: 'action', name: 'action', orderable: false, searchable: true},
            ]
        });

        //setInterval(table.draw, 30000);

        function resetForm(){
            $('#save').trigger("reset");
            $('#save_id').val('');
        }

        function getStore(val){
            $.ajax({
                url: "{{ route('setting.stores') }}",
                type: "GET",
                data: {"format": "json"},
                dataType: "json",
                success:function(data) {                    
                    $('#store').empty();
                    $('#store').append('<option value="">.:: Pilih Toko ::.</option>');
                    $.each(data, function(key, value) {
                        if(value.store_id == val){
                            $('#store').append('<option value="'+ value.store_id +'" selected>'+ value.store_name +'</option>');
                        } else {
                            
                            $('#store').append('<option value="'+ value.store_id +'">'+ value.store_name +'</option>');
                        }
                    });
                }
            });
        }

        function getStoreImport(){
            $.ajax({
                url: "{{ route('setting.stores') }}",
                type: "GET",
                data: {"format": "json"},
                dataType: "json",
                success:function(data) {                    
                    $('#store-import').empty();
                    $('#store-import').append('<option value="">.:: Pilih Toko ::.</option>');
                    $.each(data, function(key, value) {
                        $('#store-import').append('<option value="'+ value.store_id +'">'+ value.store_name +'</option>');
                    });
                }
            });
        }

        function getStoreExport(){
            $.ajax({
                url: "{{ route('setting.stores') }}",
                type: "GET",
                data: {"format": "json"},
                dataType: "json",
                success:function(data) {                    
                    $('#store-export').empty();
                    $('#store-export').append('<option value="">.:: Pilih Toko ::.</option>');
                    $.each(data, function(key, value) {
                        $('#store-export').append('<option value="'+ value.store_id +'">'+ value.store_name +'</option>');
                    });
                }
            });
        }

        function addProduct(){
            $('#modal-title').text("Tambah Produk");
            $('#button-save').text("Tambahkan");
            resetForm();
            if($('#store').length != 0){
                getStore();
            }
        }

        function saveProduct(){
            $.ajax({
                type: "post",
                data: {
                        "id":$('#save_id').val(),
                        "product_code":$('#product_code').val(),
                        "product_name":$('#product_name').val(),
                        "qty":$('#qty').val(),
                        "price":$('#price').val(),
                        "store":$('#store').val(),
                        "expired_at":$("#expired_at_txt").val(),
                },
                dataType: "json",
                url: "{{ route('products.save') }}",
                success: function (data) {
                    if(data.success){
                        toastr.success(data.success);
                    } else {
                        toastr.error(data.error);
                    }
                    table.draw();
                },
                error: function (data) {
                    if(data.responseText != null){
                        var resp = JSON.parse(data.responseText);
                        var msg = "";
                        $.each(resp.errors, function (key, val) {
                            msg += "<li>"+val+"</li>";
                        });
                        toastr.error(msg);
                    } else {
                        toastr.error(data.error);
                    }
                    console.log('Error:', data);
                }
            });
        }

        function editProduct(data){
            $('#modal-title').text("Edit Produk");
            $('#button-save').text("Simpan");
            resetForm();
            $('#save_id').val(data.product_id);
            $('#product_code').val(data.product_code);
            $('#product_name').val(data.product_name);
            $('#qty').val(data.qty);
            $('#price').val(data.price);
            $("#expired_at_txt").val(data.expired_at).change();
            if($('#store').length != 0){
                getStore(data.store_id);
            }
        }

        function deleteProductConfirm(data){
            $('#delete_id').val(data.product_id);
            $('#delete_store_name').text(data.store_name);
            $('#pcode').text(data.product_code);
        }

        function deleteProduct(){
            $.ajax({
                type: "post",
                data: {"_method":"delete", "id": $('#delete_id').val()},
                dataType: "json",
                url: "{{ route('products.delete') }}",
                success: function (data) {
                    if(data.success){
                        toastr.success(data.success);
                    } else {
                        toastr.error(data.error);
                    }
                    table.draw();
                },
                error: function (data) {
                    if(data.responseText != null){
                        var resp = JSON.parse(data.responseText);
                        var msg = "";
                        $.each(resp.errors, function (key, val) {
                            msg += "<li>"+val+"</li>";
                        });
                        toastr.error(msg);
                    } else {
                        toastr.error(data.error);
                    }
                    console.log('Error:', data);
                }
            });
        }

        function importProductForm(){
            getStoreImport();
        }

        function exportProductForm(){
            getStoreExport();
        }

        function importProduct(){
            toastr.options = {
                timeOut: 0,
                extendedTimeOut: 0,
                tapToDismiss: false
            };
            toastr.info("Mohon tunggu...");
            var form = $('#import')[0];
            var data = new FormData(form);
            $.ajax({
                type: "post",
                enctype: "multipart/form-data",
                url: "{{ route('products.import') }}",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    toastr.clear();
                    toastr.options.tapToDismiss = true;
                    if(data.success){
                        toastr.success(data.success);
                    } else {
                        toastr.error(data.error);
                    }
                    table.draw();
                    $('#import-product').modal({show: false});
                },
                error: function (data) {
                    toastr.clear();
                    toastr.options.tapToDismiss = true;
                    if(data.responseText != null){
                        var resp = JSON.parse(data.responseText);
                        var msg = "";
                        $.each(resp.errors, function (key, val) {
                            msg += "<li>"+val+"</li>";
                        });
                        toastr.error(msg);
                    } else {
                        toastr.error(data.error);
                    }
                    $('#import-product').modal({show: false});
                    console.log('Error:', data);
                }
            });
        }
    </script>
    @if(Session::has('success'))
        <script>toastr.success('{!! Session::get("success") !!}');</script>
    @endif
    @if(Session::has('error'))
        <script>toastr.error('{!! Session::get("error") !!}');</script>
    @endif
    @if(!empty($errors->all()))
        <script>toastr.error('{!! implode("", $errors->all("<li>:message</li>")) !!}');</script>
    @endif
@endsection