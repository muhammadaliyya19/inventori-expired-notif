<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['globalSettings']);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $req)
    {
        $search     =  $req->search;
        $products   = [];

        if(!empty($search)){
            $products = DB::table('products')
                        ->leftJoin("stores", "products.store_id", "stores.store_id")
                        ->select("products.*", "stores.store_name")
                        ->where("product_code", "LIKE", "%".$search."%")
                        ->orWhere("product_name", "LIKE", "%".$search."%")
                        ->get();
        }
        return View::make('home')->with(compact('products'));
    }

    public function dashboard()
    {
        if(Auth::user()->role == 0){
            $total_product = DB::table('products')->get()->count();
        } else {
            $total_product = DB::table('products')->where("store_id", Auth::user()->store_id)->get()->count();
        }
        $total_store = DB::table('stores')->get()->count();
        return View::make('dashboard')->with(compact('total_product', 'total_store'));
    }
}
