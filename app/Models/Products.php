<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = "products";
    public $timestamps = false; 
    protected $fillable = ['product_code','product_name','qty','price','expired_at'];
}
