<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  @hasSection('custom-css')
    @yield('custom-css')
  @endif
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <span class="nav-link">@yield('title')</span>
      </li>
    </ul>
    @if(Auth::check())
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
          <i class="fas fa-hourglass-end"></i>
          <span class="badge badge-danger navbar-badge"><span id="notif_expired_badge">0</span></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
          <span class="dropdown-item dropdown-header">PERHATIAN!</span>
          <a href="{{ route('products') }}?status=1" class="dropdown-item">
            Ada <span id="notif_expired" class="font-weight-bold">0</span> produk telah expired!
          </a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
          <i class="fas fa-hourglass-half"></i>
          <span class="badge badge-warning navbar-badge"><span id="notif_reminder_badge">0</span></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
          <span class="dropdown-item dropdown-header">PERHATIAN!</span>
          <a href="{{ route('products') }}?status=2" class="dropdown-item">
            Ada <span id="notif_reminder" class="font-weight-bold">0</span> produk akan segera expired!
          </a>
        </div>
      </li>
    </ul>
    @endif
  </nav>
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="brand-link text-center">
      <span class="text-white bg-gradient-warning rounded p-1 font-weight-bold text-uppercase">{{ $store_name }}</span>
    </div>
    <div class="sidebar">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
                <a class="nav-link {{ (Route::current()->getName() == 'home')? 'active':''}}" href="{{ route('home') }}">
                    <i class="nav-icon fas fa-home"></i>
                    <p class="text">{{ __('Beranda') }}</p>
                </a>
            </li>
            @if(Auth::check())
            <li class="nav-item">
                <a class="nav-link {{ (Route::current()->getName() == 'dashboard')? 'active':''}}" href="{{ route('dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p class="text">{{ __('Dasbor') }}</p>
                </a>
            </li>
            <li class="nav-header">Produk</li>
            <li class="nav-item">
                <a class="nav-link {{ (Route::current()->getName() == 'products')? 'active':''}}" href="{{ route('products') }}">
                    <i class="nav-icon fas fa-boxes"></i>
                    <p class="text">{{ __('Daftar Produk') }}</p>
                </a>
            </li>
            <li class="nav-header">{{ __('Pengaturan') }}</li>
            @if(Auth::user()->role == 0)
            <li class="nav-item">
              <a class="nav-link {{ (Route::current()->getName() == 'setting.stores')? 'active':''}}" href="{{ route('setting.stores') }}">
                  <i class="nav-icon fas fa-store"></i>
                  <p class="text">{{ __('Toko') }}</p>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ (Route::current()->getName() == 'setting.users')? 'active':''}}" href="{{ route('setting.users') }}">
                  <i class="nav-icon fas fa-users"></i>
                  <p class="text">{{ __('Pengguna') }}</p>
              </a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link {{ (Route::current()->getName() == 'setting.account')? 'active':''}}" href="{{ route('setting.account') }}">
                  <i class="nav-icon fas fa-user-cog"></i>
                  <p class="text">{{ __('Akun') }}</p>
              </a>
            </li>
            <li class="nav-item">
              <form id="logout" action="{{ route('logout') }}" method="post">@csrf</form>
              <a class="nav-link" href="javascript:;" onclick="document.getElementById('logout').submit();">
                  <i class="nav-icon fas fa-sign-out-alt text-danger"></i>
                  <p class="text">{{ __('Logout') }} ({{ Auth::user()->username }})</p>
              </a>
            </li>
            @else
            <li class="nav-item">
                <a class="nav-link {{ (Route::current()->getName() == 'login')? 'active':''}}" href="{{ route('login') }}">
                    <i class="nav-icon fas fa-sign-out-alt text-danger"></i>
                    <p class="text">{{ __('Login') }}</p>
                </a>
            </li>
            @endif
        </ul>
      </nav>
    </div>
  </aside>

  <div class="content-wrapper">
    @yield('content')
  </div>
  
  <footer class="main-footer float-right">
    <b>Version</b> {{ config('app.version') }}
  </footer>

  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>

<script src="/plugins/jquery/jquery.min.js"></script>
<script src="/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/js/adminlte.js"></script>
@hasSection('custom-js')
    @yield('custom-js')
@endif
@if(Auth::check())
<script>
  $(function () {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      expiredNotif();
      setInterval(expiredNotif, 10000);
  });
  function expiredNotif(){
      $.ajax({
          url: "{{ route('products.expiryCheck') }}",
          type: "GET",
          data: {"format": "json"},
          dataType: "json",
          success:function(data) {
              $('#notif_expired_badge').text(data.count);
              $('#notif_expired').text(data.count);
          }
      });
      $.ajax({
          url: "{{ route('products.expiryCheck') }}",
          type: "GET",
          data: {"format": "json", "interval":true},
          dataType: "json",
          success:function(data) {
              $('#notif_reminder_badge').text(data.count);
              $('#notif_reminder').text(data.count);
          }
      });
  }
</script>
@endif
</body>
</html>
