<?php

namespace App\Imports;

use App\Models\Products;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\DB;

class ProductImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $product = [
            'product_code'  => $row['KLS'],
            'product_name'  => $row['DESC'],
            'qty'           => $row['QTY'],
            'expired_at'    => $row['TGL_EXPIRED'],
        ];

        return new Products($product);
    }
}
