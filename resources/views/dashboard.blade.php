@extends('layouts.main')
@section('title', __('Dashboard'))
@section('custom-css')
    <link rel="stylesheet" href="/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endsection
@section('content')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
    </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-6">
                <a href="{{ route('products') }}?status=2">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><span id="reminder">0</span></h3>
                            <p>Segera Expired</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-hourglass-half"></i>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-6">
                <a href="{{ route('products') }}?status=1">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><span id="expired">0</span></h3>
                            <p>Telah Expired</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-hourglass-end"></i>
                        </div>
                    </div>
                </a>
            </div>
            @if(Auth::user()->role == 0)
            <div class="col-lg-3 col-6">
                <a href="{{ route('setting.stores') }}">
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3>{{ $total_store }}</h3>
                            <p>Toko</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-store"></i>
                        </div>
                    </div>
                </a>
            </div>
            @endif
            <div class="col-lg-3 col-6">
                <a href="{{ route('products') }}">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{ $total_product }}</h3>
                            <p>Total Produk</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-boxes"></i>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
@endsection
@section('custom-js')
    <script src="/plugins/toastr/toastr.min.js"></script>
    <script src="/plugins/select2/js/select2.full.min.js"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            expiredCount();
            setInterval(expiredCount, 10000);
        });

        function expiredCount(){
            $.ajax({
                url: "{{ route('products.expiryCheck') }}",
                type: "GET",
                data: {"format": "json"},
                dataType: "json",
                success:function(data) {
                    $('#expired').text(data.count);
                }
            });

            $.ajax({
                url: "{{ route('products.expiryCheck') }}",
                type: "GET",
                data: {"format": "json", "interval":true},
                dataType: "json",
                success:function(data) {
                    $('#reminder').text(data.count);
                }
            });
        }
    </script>
    @if(Session::has('success'))
        <script>toastr.success('{!! Session::get("success") !!}');</script>
    @endif
    @if(Session::has('error'))
        <script>toastr.error('{!! Session::get("error") !!}');</script>
    @endif
    @if(!empty($errors->all()))
        <script>toastr.error('{!! implode("", $errors->all("<li>:message</li>")) !!}');</script>
    @endif
@endsection