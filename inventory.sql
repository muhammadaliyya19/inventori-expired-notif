-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2021 at 10:35 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proyek3_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notif_id` bigint(255) NOT NULL,
  `user_id` bigint(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` bigint(255) NOT NULL,
  `store_id` bigint(255) NOT NULL,
  `product_code` varchar(100) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `qty` bigint(255) NOT NULL,
  `price` decimal(11,2) DEFAULT 0.00,
  `expired_at` date DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `store_id`, `product_code`, `product_name`, `qty`, `price`, `expired_at`) VALUES
(4, 5, '271712', 'TREATS JERKY BEEF FLAVOR PEDIGREE 80GR', 9, NULL, '2019-09-15'),
(5, 5, '10053096', 'RABBIT FOOD SMARTHEART VEGGIESCEREAL 1KG', 4, NULL, '2019-09-19'),
(6, 5, '201545', 'ORNAMENTAL FISH FLAKE 9IN1 45G/330ML', 11, NULL, '2019-09-22'),
(7, 5, '10070269', 'CAT FOOD 1,1KG FRISKIES KITTEN DISCOVERY', 1, NULL, '2019-09-22'),
(8, 5, '130550', 'CAT FOOD 450GR WHISKAS DRY JUNIOR OFISH', 1, NULL, '2019-09-25'),
(9, 5, '295398', 'CLEAN EYES GLD MEDAL F/ DOG&CATS 4OZ', 6, NULL, '2019-09-30'),
(10, 5, '10092849', 'BENIH MARIGOLD MAHARANI', 9, NULL, '2019-09-30'),
(11, 5, '10092850', 'BENIH MARIGOLD PERFECT MIX', 7, NULL, '2019-09-30'),
(12, 5, '10168931', 'BENIH BUNGA VINCA SOLAR FRESH RED', 9, NULL, '2019-09-30'),
(13, 5, '10092852', 'BENIH DIANTUS PERSIAN CARPET MIX', 9, NULL, '2019-09-30'),
(14, 5, '10154653', 'DOG FOOD PRIME LAMB RICE 13.2OZ', 12, NULL, '2019-10-01'),
(15, 5, '10154654', 'DOG FOOD PRIME BEEF STEW 13.2OZ', 28, NULL, '2019-10-01'),
(16, 5, '286981', 'SOFT BREAD-CHEESE&SPINACH FLV 100G', 11, NULL, '2019-10-01'),
(17, 5, '286982', 'SOFT BREAD-HONEY&SWEET POTATO FLV 100G', 9, NULL, '2019-10-01'),
(18, 5, '286983', 'SOFT BREAD-MILK FLAVOR 100G 81460', 10, NULL, '2019-10-01'),
(19, 5, '10053418', 'DOG FOOD MEAT JERKY GRILLED LIVER 60GR', 14, NULL, '2019-10-01'),
(20, 5, '10070270', 'CAT FOOD 1,1KG FRISKIES INDOOR DELIGHT', 2, NULL, '2019-10-02'),
(21, 5, '10053096', 'RABBIT FOOD SMARTHEART VEGGIESCEREAL 1KG', 12, NULL, '2019-10-02'),
(22, 5, '10070266', 'CAT FOOD 400GR FRISKIES KITTEN DISCOVERY', 2, NULL, '2019-10-04'),
(23, 5, '121987', 'CAT FOOD WHISKAS DRY POCKET TUNA 1.2 KG', 5, NULL, '2019-10-04'),
(24, 5, '121987', 'CAT FOOD WHISKAS DRY POCKET TUNA 1.2 KG', 7, NULL, '2019-10-05'),
(25, 5, '10068111', 'CAT FOOD RC FCN HAIR SKIN CARE 2 KG', 6, NULL, '2019-10-07'),
(26, 5, '271712', 'TREATS JERKY BEEF FLAVOR PEDIGREE 80GR', 16, NULL, '2019-10-08'),
(27, 5, '10070269', 'CAT FOOD 1,1KG FRISKIES KITTEN DISCOVERY', 4, NULL, '2019-10-09'),
(28, 5, '10068148', 'DOG FOOD RC LHN INDOOR LIFE JUNIOR 1.5KG', 6, NULL, '2019-10-11'),
(29, 5, '10139085', 'CAT FOOD FBN PERSIAN POUCH 85 G', 11, NULL, '2019-10-11'),
(30, 5, '193258', 'PEDIGREE POUCH PUPPYCHICKEN CIS 130GR', 10, NULL, '2019-10-12'),
(31, 5, '10068114', 'CAT FOOD RC FCN DIGESTIVE CARE 400 GR', 6, NULL, '2019-10-13'),
(32, 5, '10068124', 'CAT FOOD RC FHN BABYCAT 2 KG', 6, NULL, '2019-10-13'),
(33, 5, '10068119', 'CAT FOOD RC FBN PERSIAN 2 KG', 6, NULL, '2019-10-16'),
(34, 5, '10068131', 'DOG FOOD RC MINI STARTER 3 KG', 4, NULL, '2019-10-17'),
(35, 5, '121966', 'DOG FOOD PED CAN 5 KINDS 400 GR', 24, NULL, '2019-10-18'),
(36, 5, '10154650', 'DOG FOOD ADULT CHIC LIVER VEG 1.5KG', 6, NULL, '2019-10-19'),
(37, 5, '10068113', 'CAT FOOD RC FCN URINARY CARE 2 KG', 3, NULL, '2019-10-22'),
(38, 5, '264259', 'CATFOOD1.1KG POCKET CHICKEN TUNA WHISKAS', 3, NULL, '2019-10-24'),
(39, 5, '194510', 'DOG FOOD DRY ALPO ADULT BEEF 1500 GR', 6, NULL, '2019-10-25'),
(40, 5, '10068117', 'CAT FOOD RC FBN KITTEN PERSIAN 2 KG', 4, NULL, '2019-10-27'),
(41, 5, '10168934', 'BENIH CABAI ORNAMENTAL LA', 6, NULL, '2019-10-30'),
(42, 5, '10168929', 'BENIH CABAI BESAR HOT CHILLI F1', 5, NULL, '2019-10-30'),
(43, 5, '10168930', 'BENIH LABU KUNING SUPREMA F1', 10, NULL, '2019-10-30'),
(44, 5, '216481', 'DOGFOOD COUNTRY BEEF 2 KG', 1, NULL, '2019-10-31'),
(45, 5, '216481', 'DOGFOOD COUNTRY BEEF 2 KG', 5, NULL, '2019-10-31'),
(46, 5, '295402', 'EAR MITE & TICK CTRL GLD MEDAL 4OZ 40504', 3, NULL, '2019-10-31'),
(47, 5, '134366', 'DOG FOOD PED DRY PUPPY CHICK&EGG8 KG', 1, NULL, '2019-10-31'),
(48, 5, '10107467', 'BENIH BALSAM TOM TOM MIXED ', 2, NULL, '2019-10-31'),
(49, 5, '10168931', 'BENIH BUNGA VINCA SOLAR FRESH RED', 10, NULL, '2019-10-31'),
(50, 5, '302026', 'MEDICATED SHAMPOO GOLD MEDAL 17OZ', 2, NULL, '2019-11-01'),
(51, 5, '302027', 'TEA TREE SHAMPOO 17OZ 10250', 4, NULL, '2019-11-01'),
(52, 5, '276165', 'DOGFOOD PEDIGREE 1.5K SML BREED 112777', 5, NULL, '2019-11-02'),
(53, 5, '10070266', 'CAT FOOD 400GR FRISKIES KITTEN DISCOVERY', 2, NULL, '2019-11-08'),
(54, 5, '10070268', 'CAT FOOD 450GR FRISKIES SEAFD SENSATION', 5, NULL, '2019-11-09'),
(55, 5, '10062273', 'DOG FOOD CESAR BEFF 100GR', 23, NULL, '2019-11-15'),
(56, 5, '10068113', 'CAT FOOD RC FCN URINARY CARE 2 KG', 2, NULL, '2019-11-16'),
(57, 5, '10070268', 'CAT FOOD 450GR FRISKIES SEAFD SENSATION', 3, NULL, '2019-11-18'),
(58, 5, '10070268', 'CAT FOOD 450GR FRISKIES SEAFD SENSATION', 1, NULL, '2019-11-21'),
(59, 5, '10037262', 'DOG FOOD ALPO ADULT LAMB VEG 1.5 KG', 6, NULL, '2019-11-22'),
(60, 5, '264257', 'TREAT - DENTA STIX PUPPY PEDIGREE 56GR', 48, NULL, '2019-11-22'),
(61, 5, '10092836', 'BENIH PAKCOI NAULI', 3, NULL, '2019-11-30'),
(62, 5, '10168929', 'BENIH CABAI BESAR HOT CHILLI F1', 10, NULL, '2019-11-30'),
(63, 5, '10092853', 'BENIH HELIANTHUS COCOCSUN', 4, NULL, '2019-11-30'),
(64, 5, '136524', 'CATFOOD WHISKAS DRY 1.2 KG POCK MACKEREL', 5, NULL, '2019-12-01'),
(65, 5, '10154652', 'DOG FOOD GRAVY T-BONE STEAK 13OZ', 25, NULL, '2019-12-01'),
(66, 5, '271712', 'TREATS JERKY BEEF FLAVOR PEDIGREE 80GR', 48, NULL, '2019-12-04'),
(67, 5, '281526', 'GOLD FISH PELLET 900 ML AZ80103', 2, NULL, '2019-12-05'),
(68, 5, '10070274', 'CAT FOOD 3KG FRISKIES SEAFOOD SENSATIONS', 4, NULL, '2019-12-07'),
(69, 5, '206693', 'TREATS DENTA STIX S PEDIGREE 75GR YLW', 18, NULL, '2019-12-07'),
(70, 5, '276161', 'DOG FOOD PDG DRY CKN & VEG 1.5KG', 6, NULL, '2019-12-13'),
(71, 5, '264259', 'CATFOOD1.1KG POCKET CHICKEN TUNA WHISKAS', 1, NULL, '2019-12-18'),
(72, 5, '10068133', 'DOG FOOD RC SHN XSMALL ADULT 1.5 KG', 8, NULL, '2019-12-21'),
(73, 5, '10154434', 'DOG FOOD POUCH BEEF AND CHIC GRAVY 130GR', 12, NULL, '2019-12-24'),
(74, 5, '10068115', 'CAT FOOD RC FCN DIGESTIVE CARE 2 KG', 6, NULL, '2019-12-25'),
(75, 5, '160353', 'CAT FOOD 1.1 KG WHISKAS DRY JUNIOR OFISH', 5, NULL, '2019-12-25'),
(76, 5, '10068129', 'CAT FOOD RC ADULT INSTINC GRAVY PCH 85GR', 38, NULL, '2019-12-29'),
(77, 5, '10068128', 'CAT FOOD RC KITEN INSTINC GRAVY PCH 85GR', 0, NULL, '2019-12-30'),
(78, 5, '10068128', 'CAT FOOD RC KITEN INSTINC GRAVY PCH 85GR', 9, NULL, '2019-12-30'),
(79, 5, '276164', 'DOG FOOD PDG DRY BEEF & VEG 3KG', 4, NULL, '2019-12-31'),
(80, 5, '286980', 'CAT SNACK TUNA 45G Z1506', 10, NULL, '2020-01-01'),
(81, 5, '286985', 'CAT SNACK-FISH SAUSAGE 45G Z1507', 10, NULL, '2020-01-01'),
(82, 5, '10011446', 'INSEKTISIDA SMASH 100EC 100ML', 12, NULL, '2020-01-01'),
(83, 5, '10154435', 'DOG FOOD POUCH LIVER VEG GRAVY 130GR', 6, NULL, '2020-01-06'),
(84, 5, '238678', 'CATFOOD 85GR POUCH JUNIOR MACK WHISKAS', 24, NULL, '2020-01-06'),
(85, 5, '10154605', 'CAT FOOD WET KITTEN TUNA POUCH 80GR', 6, NULL, '2020-01-11'),
(86, 5, '10070272', 'CAT FOOD 1,2KG FRISKIES SURFIN TURFIN', 5, NULL, '2020-01-12'),
(87, 5, '10070273', 'CAT FOOD 1,2KG FRISKIES SEAFOD SENSATION', 5, NULL, '2020-01-16'),
(88, 5, '134366', 'DOG FOOD PED DRY PUPPY CHICK&EGG8 KG', 1, NULL, '2020-01-22'),
(89, 5, '10068149', 'DOGFOOD SHN MINI INDOOR LIFE ADULT 1.5KG', 6, NULL, '2020-01-24'),
(90, 5, '264259', 'CATFOOD1.1KG POCKET CHICKEN TUNA WHISKAS', 8, NULL, '2020-01-29'),
(91, 5, '10092827', 'BENIH CABAI KERITING CASTILLO', 10, NULL, '2020-01-30'),
(92, 5, '10107465', 'BENIH PARIA LIPA F1', 7, NULL, '2020-01-30'),
(93, 5, '286946', 'DELICIOUS CHICKEN JERKY 380G Z0016', 6, NULL, '2020-01-30'),
(94, 5, '160350', 'DOG FOOD CESAR TRAY TENDERLAMP 100GR', 11, NULL, '2020-01-31'),
(95, 5, '295394', 'WHITENING SHAMPOO GOLD MEDAL 17OZ 10050', 1, NULL, '2020-02-01'),
(96, 5, '10053096', 'RABBIT FOOD SMARTHEART VEGGIESCEREAL 1KG', 12, NULL, '2020-02-05'),
(97, 5, '10046303', 'PDG SMALL BREED CHICKEN, LIVER VEG 1,5KG', 6, NULL, '2020-02-11'),
(98, 5, '10046303', 'PDG SMALL BREED CHICKEN, LIVER VEG 1,5KG', 6, NULL, '2020-02-11'),
(99, 5, '160353', 'CAT FOOD 1.1 KG WHISKAS DRY JUNIOR OFISH', 6, NULL, '2020-02-12'),
(100, 5, '10154649', 'CAT FOOD WET TUNA MACK POUCH 80GR', 22, NULL, '2020-02-13'),
(101, 5, '276162', 'DOG FOOD DRY PEDIGREE CHICKEN & VEG 3KG', 6, NULL, '2020-02-13'),
(102, 5, '160350', 'DOG FOOD CESAR TRAY TENDERLAMP 100GR', 11, NULL, '2020-02-15'),
(103, 5, '10154606', 'CAT FOOD WET TUNA POUCH 80GR', 18, NULL, '2020-02-15'),
(104, 5, '10053418', 'DOG FOOD MEAT JERKY GRILLED LIVER 60GR', 12, NULL, '2020-02-17'),
(105, 5, '10068111', 'CAT FOOD RC FCN HAIR SKIN CARE 2 KG', 6, NULL, '2020-02-20'),
(106, 5, '276165', 'DOGFOOD PEDIGREE 1.5K SML BREED 112777', 6, NULL, '2020-02-22'),
(107, 5, '10068128', 'CAT FOOD RC KITEN INSTINC GRAVY PCH 85GR', 3, NULL, '2020-02-24'),
(108, 5, '160350', 'DOG FOOD CESAR TRAY TENDERLAMP 100GR', 24, NULL, '2020-02-27'),
(109, 5, '295392', 'PUPPY SHAMPOO GOLD MEDAL 17OZ 18050', 3, NULL, '2020-02-28'),
(110, 5, '238674', 'CATFOOD WHISKAS PCH 85 GR OFISH 112295', 13, NULL, '2020-02-28'),
(111, 5, '10070269', 'CAT FOOD 1,1KG FRISKIES KITTEN DISCOVERY', 4, NULL, '2020-02-28'),
(112, 5, '10122920', 'DENTASTIX PEDIGREE LARGE 112 GR', 24, NULL, '2020-02-28'),
(113, 5, '10092830', 'BENIH TOMAT TYMOTI', 9, NULL, '2020-02-28'),
(114, 5, '286945', 'DELICIOUS CHICKEN JERKY 180G Z0014', 6, NULL, '2020-02-28'),
(115, 5, '286981', 'SOFT BREAD-CHEESE&SPINACH FLV 100G', 36, NULL, '2020-02-28'),
(116, 5, '10107467', 'BENIH BALSAM TOM TOM MIXED ', 10, NULL, '2020-02-28'),
(117, 5, '10092853', 'BENIH HELIANTHUS COCOCSUN', 10, NULL, '2020-02-28'),
(118, 5, '10092852', 'BENIH DIANTUS PERSIAN CARPET MIX', 10, NULL, '2020-02-28'),
(119, 5, '286983', 'SOFT BREAD-MILK FLAVOR 100G 81460', 32, NULL, '2020-02-28'),
(120, 5, '286946', 'DELICIOUS CHICKEN JERKY 380G Z0016', 6, NULL, '2020-02-29'),
(121, 5, '10070273', 'CAT FOOD 1,2KG FRISKIES SEAFOD SENSATION', 3, NULL, '2020-02-29'),
(122, 5, '134355', 'CATFOOD WHISKAS PCH 85GR SALMON MACAREL', 42, NULL, '2020-03-01'),
(123, 5, '10139085', 'CAT FOOD FBN PERSIAN POUCH 85 G', 12, NULL, '2020-03-02'),
(124, 5, '134366', 'DOG FOOD PED DRY PUPPY CHICK&EGG8 KG', 1, NULL, '2020-03-06'),
(125, 5, '10154605', 'CAT FOOD WET KITTEN TUNA POUCH 80GR', 33, NULL, '2020-03-17'),
(126, 5, '276162', 'DOG FOOD DRY PEDIGREE CHICKEN & VEG 3KG', 3, NULL, '2020-03-18'),
(127, 5, '10154607', 'CAT FOOD WET TUNA SARD POUCH 80GR', 19, NULL, '2020-03-19'),
(128, 5, '10070266', 'CAT FOOD 400GR FRISKIES KITTEN DISCOVERY', 2, NULL, '2020-03-24'),
(129, 5, '10092840', 'BENIH SELADA GRAND RAPIDS', 8, NULL, '2020-03-30'),
(130, 5, '10092836', 'BENIH PAKCOI NAULI', 10, NULL, '2020-03-30'),
(131, 5, '10168932', 'BENIH KANGKUNG BANGKOK LP-1', 9, NULL, '2020-03-30'),
(132, 5, '10168933', 'BENIH BUNGA IMPATIENS BALANCE VIOLET STA', 10, NULL, '2020-03-30'),
(133, 5, '302031', 'HOUSEBREAKING AID F/PUPPIES 2OZ 41502', 1, NULL, '2020-04-01'),
(134, 5, '223091', 'NOVOPEARL JBL 100ML D/GB YLW 3029900', 5, NULL, '2020-04-01'),
(135, 5, '10068110', 'CAT FOOD RC FCN HAIR SKIN CARE 400GR', 12, NULL, '2020-04-02'),
(136, 5, '10122920', 'DENTASTIX PEDIGREE LARGE 112 GR', 24, NULL, '2020-04-08'),
(137, 5, '10068126', 'CAT FOOD RC FHN KITTEN 2 KG', 6, NULL, '2020-04-10'),
(138, 5, '206690', 'DOG FOOD PEDIGREE BEEF 20 KG', 3, NULL, '2020-04-22'),
(139, 5, '271712', 'TREATS JERKY BEEF FLAVOR PEDIGREE 80GR', 48, NULL, '2020-04-22'),
(140, 5, '10070272', 'CAT FOOD 1,2KG FRISKIES SURFIN TURFIN', 1, NULL, '2020-04-24'),
(141, 5, '194522', 'WET CAT FRISKIES PURETUNA 400 GR**', 19, NULL, '2020-04-28'),
(142, 5, '194522', 'WET CAT FRISKIES PURETUNA 400 GR**', 1, NULL, '2020-04-28'),
(143, 5, '10070273', 'CAT FOOD 1,2KG FRISKIES SEAFOD SENSATION', 5, NULL, '2020-04-29'),
(144, 5, '10092829', 'BENIH CABAI RAWIT PELITA', 10, NULL, '2020-04-30'),
(145, 5, '10092829', 'BENIH CABAI RAWIT PELITA', 10, NULL, '2020-04-30'),
(146, 5, '264234', 'TREATS JERKY LAMB FLAVOR PEDIGREE 80GR', 48, NULL, '2020-04-30'),
(147, 5, '10168934', 'BENIH CABAI ORNAMENTAL LA', 10, NULL, '2020-04-30'),
(148, 5, '286961', 'GRN DENTAL W/ CKNEN FILLET 60G 81279', 15, NULL, '2020-05-01'),
(149, 5, '134353', 'CAT FOOD WHISKAS POUCH TUNA 85GR', 42, NULL, '2020-05-01'),
(150, 5, '238679', 'WHISKAS POUCH CHICKEN TUNA 85 GR', 49, NULL, '2020-05-01'),
(151, 5, '10183506', 'CAT FOOD CAN OCEAN PLATTER 400GR', 5, NULL, '2020-05-04'),
(152, 5, '10180232', 'DOG FOOD POUCH SIMMERED BEEF VEG 80GR', 24, NULL, '2020-05-04'),
(153, 5, '10068136', 'DOG FOOD RC SHN MINI ADULT 2 KG', 6, NULL, '2020-05-09'),
(154, 5, '10180233', 'DOG FOOD POUCH CHICKEN&GRILLED VEG 80GR', 22, NULL, '2020-05-10'),
(155, 5, '10180231', 'DOG FOOD POUCH ROASTED BEEF VEG 80GR', 20, NULL, '2020-05-11'),
(156, 5, '206692', 'TREATS DENTA STIX M/L PEDIGREE 98GR YLW', 24, NULL, '2020-05-14'),
(157, 5, '206693', 'TREATS DENTA STIX S PEDIGREE 75GR YLW', 24, NULL, '2020-05-16'),
(158, 5, '238678', 'CATFOOD 85GR POUCH JUNIOR MACK WHISKAS', 24, NULL, '2020-05-16'),
(159, 5, '281526', 'GOLD FISH PELLET 900 ML AZ80103', 7, NULL, '2020-05-19'),
(160, 5, '238679', 'WHISKAS POUCH CHICKEN TUNA 85 GR', 24, NULL, '2020-05-22'),
(161, 5, '201549', 'MICRO PELLET 9IN1 35ML', 4, NULL, '2020-05-26'),
(162, 5, '286978', 'CAT SNACK-CRAB & CHICKEN SLICE 38G Z1509', 12, NULL, '2020-05-30'),
(163, 5, '286979', 'CAT SNACK-FISH & CHICKEN SLICE 38G Z1508', 12, NULL, '2020-05-30'),
(164, 5, '286946', 'DELICIOUS CHICKEN JERKY 380G Z0016', 6, NULL, '2020-05-30'),
(165, 5, '10092828', 'BENIH CABAI RAWIT DEWATA', 10, NULL, '2020-05-30'),
(166, 5, '286945', 'DELICIOUS CHICKEN JERKY 180G Z0014', 6, NULL, '2020-05-30'),
(167, 5, '286978', 'CAT SNACK-CRAB & CHICKEN SLICE 38G Z1509', 24, NULL, '2020-05-30'),
(168, 5, '286980', 'CAT SNACK TUNA 45G Z1506', 12, NULL, '2020-05-30'),
(169, 5, '10168934', 'BENIH CABAI ORNAMENTAL LA', 10, NULL, '2020-05-30'),
(170, 5, '130550', 'CAT FOOD 450GR WHISKAS DRY JUNIOR OFISH', 15, NULL, '2020-06-07'),
(171, 5, '193257', 'PEDIGREE POUCH BEEF CIS 130GR', 12, NULL, '2020-06-15'),
(172, 5, '10180230', 'DOG FOOD POUCH PUPPY CHIC LIVER VEG 80GR', 22, NULL, '2020-06-26'),
(173, 5, '10107464', 'BENIH SELEDRI AMIGO ', 10, NULL, '2020-06-30'),
(174, 5, '286973', 'WHITE PIGGY GUM MILK FLAVOR 81360', 6, NULL, '2020-07-01'),
(175, 5, '136524', 'CATFOOD WHISKAS DRY 1.2 KG POCK MACKEREL', 12, NULL, '2020-07-18'),
(176, 5, '238675', 'CATFOOD WHISKAS PCH  85 GR MCKRL 112296', 16, NULL, '2020-07-25'),
(177, 5, '10092831', 'BENIH TOMAT REWAKO', 10, NULL, '2020-07-30'),
(178, 5, '302031', 'HOUSEBREAKING AID F/PUPPIES 2OZ 41502', 6, NULL, '2020-07-31'),
(179, 5, '286972', 'WHITE PIGGY GUM MILK FLAVOR MINI 81359', 7, NULL, '2020-08-01'),
(180, 5, '238676', 'CATFOOD WHISKAS POUCH 85GR TUNA & FISH', 45, NULL, '2020-08-01'),
(181, 5, '238678', 'CATFOOD 85GR POUCH JUNIOR MACK WHISKAS', 31, NULL, '2020-08-01'),
(182, 5, '10183506', 'CAT FOOD CAN OCEAN PLATTER 400GR', 24, NULL, '2020-08-03'),
(183, 5, '10183505', 'CAT FOOD CAN MACKEREL 400GR', 24, NULL, '2020-08-15'),
(184, 5, '286973', 'WHITE PIGGY GUM MILK FLAVOR 81360', 6, NULL, '2020-08-31'),
(185, 5, '133135', 'ABATE 1 G LARVASIDA (5PC)', 20, NULL, '2020-09-07'),
(186, 5, '238674', 'CATFOOD WHISKAS PCH 85 GR OFISH 112295', 24, NULL, '2020-09-09'),
(187, 5, '10068130', 'CAT FOOD RC BABY CAT INSTINC CAN 195 GR', 12, NULL, '2020-09-10'),
(188, 5, '238675', 'CATFOOD WHISKAS PCH  85 GR MCKRL 112296', 24, NULL, '2020-09-16'),
(189, 5, '238678', 'CATFOOD 85GR POUCH JUNIOR MACK WHISKAS', 24, NULL, '2020-09-16'),
(190, 5, '238675', 'CATFOOD WHISKAS PCH  85 GR MCKRL 112296', 0, NULL, '2020-09-21'),
(191, 5, '238679', 'WHISKAS POUCH CHICKEN TUNA 85 GR', 48, NULL, '2020-09-22'),
(192, 5, '201565', 'AZOO GROWTH PLUS KOI FOOD', 12, NULL, '2020-09-28'),
(193, 5, '134356', 'CAT FOOD 85GR WHISKAS POUCH JUNIOR TUNA', 7, NULL, '2020-09-28'),
(194, 5, '295396', 'FLEA&TICK SHAMPOO GLD MEDAL 17OZ 14050', 2, NULL, '2020-09-30'),
(195, 5, '238678', 'CATFOOD 85GR POUCH JUNIOR MACK WHISKAS', 24, NULL, '2020-10-02'),
(196, 5, '10154435', 'DOG FOOD POUCH LIVER VEG GRAVY 130GR', 12, NULL, '2020-10-04'),
(197, 5, '121968', 'DOG FOOD PED CAN BEEF 1.15 KG', 12, NULL, '2020-10-11'),
(198, 5, '134356', 'CAT FOOD 85GR WHISKAS POUCH JUNIOR TUNA', 17, NULL, '2020-10-19'),
(199, 5, '286970', 'WHITE PIGGY GUM MILK FLAVOR S 81357', 6, NULL, '2020-11-30'),
(200, 5, '286972', 'WHITE PIGGY GUM MILK FLAVOR MINI 81359', 12, NULL, '2020-11-30'),
(201, 5, '286973', 'WHITE PIGGY GUM MILK FLAVOR 81360', 6, NULL, '2020-11-30'),
(202, 5, '286970', 'WHITE PIGGY GUM MILK FLAVOR S 81357', 6, NULL, '2020-11-30'),
(203, 5, '10070266', 'CAT FOOD 400GR FRISKIES KITTEN DISCOVERY', 3, NULL, '2020-12-15'),
(204, 5, '10138223', 'WINDOWSILL KIT STRAWBERRY AND MINT', 3, NULL, '2020-12-30'),
(205, 5, '10138224', 'WINDOWSILL KIT LAVENDER AND THYME', 5, NULL, '2020-12-30'),
(206, 5, '10016714', 'BIO POTS YELLOW SUNFLOWER', 45, NULL, '2020-12-30'),
(207, 5, '311229', 'BIO POTS RED TOMATOES', 50, NULL, '2020-12-30'),
(208, 5, '10138222', 'BIO POT ROSEMARY GROW KIT', 2, NULL, '2020-12-30'),
(209, 5, '225668', 'RACUN TIKUS RATGON 100 GR', 10, NULL, '2020-12-30'),
(210, 5, '295391', 'OATMEAL SOOTHING SHAMPOO GLD MEDAL 17OZ', 6, NULL, '2021-01-01'),
(211, 5, '295394', 'WHITENING SHAMPOO GOLD MEDAL 17OZ 10050', 2, NULL, '2021-01-01'),
(212, 5, '10033352', 'RACUN TIKUS RACUMIN BLOCK 50G', 12, NULL, '2021-01-10'),
(213, 5, '201559', 'TURTLE STICKS SML 9IN1 33', 12, NULL, '2021-09-28'),
(214, 5, '311228', 'BIO POTS GREEN MINT', 72, NULL, '2021-12-30'),
(215, 5, '312145', 'PENGHILANG BAU SMELL OUT F/ PETS 500ML', 27, NULL, '2022-02-13'),
(216, 5, '10062278', 'SMELL OUT REFILL FOR PETS', 9, NULL, '2022-02-14'),
(217, 5, '10062278', 'SMELL OUT REFILL FOR PETS', 6, NULL, '2022-10-22'),
(218, 5, '10084244', 'INSECTISIDA CONANT 1,75 GR', 12, NULL, '2023-11-30');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `store_id` bigint(255) NOT NULL,
  `store_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`store_id`, `store_name`) VALUES
(4, 'Toko 1'),
(5, 'Toko 2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(1) NOT NULL DEFAULT 1 COMMENT '0 = Admin; 1 = User; 2 = Guest;',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `store_id` bigint(255) DEFAULT NULL,
  `exp_reminder` int(3) NOT NULL DEFAULT 7
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role`, `username`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `store_id`, `exp_reminder`) VALUES
(1, 'Admin', 0, 'admin', NULL, NULL, '$2y$10$hz/QDCEiIUUxlZwfwkdRJeUTn5PMbhFBjkKGChHfoCcBHBX5GWeAu', 'UkmLgUB4kl3y5nuN0ULuRrqw0b64Y3iBoIkzvkHIoJOpLZ1fe7rwpx4t8Gn7', '2021-02-26 09:55:13', '2021-02-26 09:55:13', NULL, 7),
(5, 'Desman Harianto Pardosi', 1, 'desmanpardosi', NULL, NULL, '$2y$10$SOTXm5Cdxs8Rj1Zjy4EF4.Fjg9qI5z.D.UfSkLZ.uSG3Vp9mc.rsC', 'XUkgRnvpjdBrNm3KSLMpMtIyeuK3F0xferyu8W49Ro1WeaitonkZ0FUj8fmw', NULL, NULL, 4, 3),
(6, 'Desman Harianto Pardosi', 1, 'desmanpardosi2', NULL, NULL, '$2y$10$k1/xCfupeQ.1qo7lVNrF8u9ENHmQ15xKEX/W9BpIa7hD4w6q5nbF6', 'feUyfyUSyIGtXYbjQmVwgqHmyx2DlKCif3gihTFknKK9bjNNKo1jLPOXW7uc', NULL, NULL, 5, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notif_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `username_2` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notif_id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `store_id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
