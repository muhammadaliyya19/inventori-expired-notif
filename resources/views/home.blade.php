@extends('layouts.main')
@section('title', __('Home'))
@section('content')
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
    </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 mx-auto mb-5">
                <form id="search" method="get">
                    <div class="input-group input-group-lg">
                        <input type="text" class="form-control" name="search" placeholder="Pencarian Produk">
                        <span class="input-group-append">
                            <button type="button" class="btn btn-success btn-flat" onclick="$('#search').submit()">Cari</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        @if(count($products) > 0)
            @php
                $i = 1;
            @endphp
            <div class="row">
            @foreach($products as $key => $d)
                <div class="col-sm-4 d-flex">
                    <div class="card border-secondary">
                        <div class="card-header">
                            <h3 class="card-title">{{ $d->store_name }}</h3>
                        </div>
                        <div class="card-body text-secondary">
                            <div class="row">
                                <div class="col-sm-4">Kode Produk</div>
                                <div class="col-sm-1">:</div>
                                <div class="col-sm-7 font-weight-bold">{{ $d->product_code }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">Nama Produk</div>
                                <div class="col-sm-1">:</div>
                                <div class="col-sm-7 font-weight-bold">{{ $d->product_name }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">Harga</div>
                                <div class="col-sm-1">:</div>
                                <div class="col-sm-7 font-weight-bold">Rp {{ number_format($d->price, 2, ",", ".") }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">Tgl. Expired</div>
                                <div class="col-sm-1">:</div>
                                <div class="col-sm-7 font-weight-bold">{{ date("d/m/Y", strtotime($d->expired_at)) }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @if($i % 3 == 0)
                </div>
                <div class="row">
            @endif
            @php
                $i++;
            @endphp
            @endforeach
        @endif
    </div>
</section>
@endsection