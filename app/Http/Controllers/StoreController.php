<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'globalSettings']);
    }
    
    public function stores(Request $req){
        if($req->format == "json"){
            $stores = DB::table('stores')->select("*")->get();
            return response()->json($stores);
        } else {
            $stores = DB::table('stores')->select("*")->paginate(50);
            return View::make("stores")->with(compact("stores"));
        }
    }

    public function store_save(Request $req){
        $store_id = $req->store_id;

        $req->validate([
            'store_name'        => 'required|min:3',
        ],
        [
            'store_name.required'       => 'Nama Toko belum diisi!',
            'store_name.min'            => 'Nama Toko minimal 3 karakter!',
        ]);

        $data = [
            "store_name"        => $req->store_name,
        ];

        if(empty($store_id)){
            $add = DB::table('stores')->insertGetId($data);

            if($add){
                $req->session()->flash('success', "Toko baru berhasil ditambahkan.");
            } else {
                $req->session()->flash('error', "Toko baru gagal ditambahkan!");
            }
        } else {
            $edit = DB::table('stores')->where("store_id", $store_id)->update($data);

            if($edit){
                $req->session()->flash('success', "Toko berhasil diubah.");
            } else {
                $req->session()->flash('error', "Toko gagal diubah!");
            }
        }
        
        return redirect()->back();
    }

    public function store_delete(Request $req){
        $del = DB::table('stores')->where("store_id", $req->delete_store_id)->delete();

        if($del){
            $storeProduct = DB::table('products')->where("store_id", $req->delete_store_id)->get()->count();
            if($storeProduct > 0){
                $delProduct = DB::table('products')->where("store_id", $req->delete_store_id)->delete();
                if($delProduct){
                    $req->session()->flash('success', "Toko dan semua produknya berhasil dihapus.");
                } else {
                    $req->session()->flash('warning', "Toko berhasil dihapus, namun produknya gagal dihapus!");
                }
            } else {
                $req->session()->flash('success', "Toko berhasil dihapus.");
            }
            
        } else {
            $req->session()->flash('error', "Toko gagal dihapus!");
        }

        return redirect()->back();
    }
}
