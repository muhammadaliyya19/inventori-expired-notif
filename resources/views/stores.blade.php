@extends('layouts.main')
@section('title', __('Toko'))
@section('custom-css')
    <link rel="stylesheet" href="/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
        </div>
        </div>
    </div>
    <section class="content">
    <div class="container-fluid">
        <div class="card">
        <div class="card-header">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-store" onclick="addStore()"><i class="fas fa-plus"></i> Tambah Toko</button>
        </div>
        <div class="card-body">
            <table id="table" class="table table-sm table-bordered table-hover table-striped">
            <thead>
                <tr class="text-center">
                    <th>No.</th>
                    <th>{{ __('Nama Toko') }}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @if(count($stores) > 0)
                @foreach($stores as $key => $d)
                @php
                    $data = ["store_id" => $d->store_id, "store_name" => $d->store_name];
                @endphp
                <tr>
                    <td class="text-center">{{ $stores->firstItem() + $key }}</td>
                    <td>{{ $data['store_name'] }}</td>
                    <td class="text-center"><button title="Edit Toko" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#add-store" onclick="editStore({{ json_encode($data) }})"><i class="fas fa-edit"></i></button> <button title="Hapus Toko" type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-store" onclick="deleteStore({{ json_encode($data) }})"><i class="fas fa-trash"></i></button></td>
                </tr>
                @endforeach
            @else
                <tr class="text-center">
                    <td colspan="4">{{ __('Belum ada.') }}</td>
                </tr>
            @endif
            </tbody>
            </table>
        </div>
        </div>
        <div>
        {{ $stores->links("pagination::bootstrap-4") }}
        </div>
    </div>
    <div class="modal fade" id="add-store">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modal-title" class="modal-title">{{ __('Tambah Toko') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form role="form" id="update" action="{{ route('setting.store.save') }}" method="post">
                        @csrf
                        <input type="hidden" id="store_id" name="store_id">
                        <div class="form-group row">
                            <label for="store_name" class="col-sm-4 col-form-label">{{ __('Nama Toko') }}</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="store_name" name="store_name">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="button-save" type="button" class="btn btn-primary" onclick="$('#update').submit();">{{ __('Tambahkan') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete-store">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modal-title" class="modal-title">{{ __('Hapus Toko') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form role="form" id="delete" action="{{ route('setting.store.delete') }}" method="post">
                        @csrf
                        @method('delete')
                        <input type="hidden" id="delete_store_id" name="delete_store_id">
                    </form>
                    <div>
                        <p>Anda yakin ingin menghapus toko <span id="delete_store_name" class="font-weight-bold"></span>?</p>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Batal') }}</button>
                    <button id="button-delete" type="button" class="btn btn-danger" onclick="$('#delete').submit();">{{ __('Ya, hapus') }}</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('custom-js')
    <script src="/plugins/toastr/toastr.min.js"></script>
    <script src="/plugins/select2/js/select2.full.min.js"></script>
    <script>
        $(function () {
            $('.select2').select2({
                theme: 'bootstrap4'
            });
        });

        function resetForm(){
            $('#update').trigger("reset");
            $('#store_id').val('');
        }

        function addStore(){
            resetForm();
            $('#modal-title').text("Tambah Toko Baru");
            $('#button-save').text("Tambahkan");
        }

        function editStore(data){
            resetForm();
            $('#modal-title').text("Edit Toke");
            $('#button-save').text("Update");
            $('#store_id').val(data.store_id);
            $('#store_name').val(data.store_name);
        }

        function deleteStore(data){
            $('#delete_store_id').val(data.store_id);
            $('#delete_store_name').text(data.store_name);
        }
    </script>
    <script src="/plugins/toastr/toastr.min.js"></script>
    @if(Session::has('success'))
        <script>toastr.success('{!! Session::get("success") !!}');</script>
    @endif
    @if(Session::has('warning'))
        <script>toastr.warning('{!! Session::get("warning") !!}');</script>
    @endif
    @if(Session::has('error'))
        <script>toastr.error('{!! Session::get("error") !!}');</script>
    @endif
    @if(!empty($errors->all()))
        <script>toastr.error('{!! implode("", $errors->all("<li>:message</li>")) !!}');</script>
    @endif
@endsection